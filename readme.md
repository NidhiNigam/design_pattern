# Design Pattern
Design Pattern is evolved as reusable solutions to the problems that we encounter every day in programming.
They are generally targeted at solving the problems of object generation and integration.

Design pattern acts as templates that can be applied to real-world programming problems.


# Types of Design pattern 

1. Creational Design Pattern
2. Structural Design Pattern
3. Behavioural Design Pattern
4. Concurrency Design Pattern
5. Architectural Design Pattern

#### 1. Creational Design Pattern

Creational Design Pattern deals with Object creation and initialization. Creational Design Pattern gives the program more flexibility in deciding which objects need to be created for a given case. 
there are some of the popular design patterns in Creational design patterns:

* Singleton
* Factory
* Abstract 
* Builder
* Prototype

### Singleton Design Pattern

Singleton pattern belongs to creational type pattern.

This pattern is used when we need to ensure that only one object of a particular class needs to be created. All further references to the object are referred to the same underlying instance created. 

### Factory Design Pattern 

Define an interface for creating an object, but let subclasses decide which class to instantiate. The Factory method lets a class defer instantiation it uses to subclasses"

Factory Pattern is one of the most used design patterns in real-world applications

Factory Pattern Creates object without exposing the creation logic to the client and refers to a newly created object using a common interface

### Abstract Design Pattern

The Abstract Factory Pattern Provides an interface for creating families of related or dependent objects without specifying their concrete classes.
Abstract Factory pattern belongs to creational patterns and is one of the most used design patterns in real-world applications.
An abstract factory is a super factory that creates other factories.

### Builder Design Pattern

Builder Design Pattern belongs to a creational pattern.
Separate the construction of a complex object from its representation so that the same construction process can create different representations.

### Prototype

Prototype Design Pattern specifies the kind of objects to create using a prototypical instance, and create new objects by copying this prototype
To simplify, Instead of creating the object from scratch every time, we can make copies of an original instance and modify it as required.
the prototype is unique among the other creational patterns as it doesn't require a class but only an end object.


### 2. Structural Design Pattern :

Structural Design Patterns are Design Patterns that ease the design by identifing a simple way to realize relationships between entities.

Flexible interconnecting modules which can work together in a larger system

Structural Patterns describe how classes and objects larger structures

There are Seven Structural Design Pattern :

* Adapter
* Bridge
* Composite
* Decorator
* Facade
* Flyweight
* Proxy

### Adapter Design Pattern

"Match interfaces of Different classes"

The Adapter design pattern allows incompatible classes to interact with each other by converting the interface of one class into an interface expected by clients
Leveraging on Adapter pattern Improves the reusability of older functionality.

### Bridge Design Pattern

Decouple an abstraction from its implementation so that the two can vary independently"
This pattern is used to separate abstraction from its implementation so that both can be modified independently.
This pattern involves an interface that acts as a bridge between the abstraction class and implementer classes
with a Bridge pattern both types of classes can be modified without affecting each other.

### Composition Design Pattern

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly"
A thing made up of several parts or elements
The composite pattern is a partitioning design pattern and it describes that the group of objects is treated the same way as a single instance of the same type of object.

### Decorator Design Pattern

Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to sub-classing for extending functionality"
Falls under the category of structural Design Pattern and is also known as Wrapper.

### Facade Design Pattern

Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use"
Falls under the category of Structural Design Pattern
Evolved from French word facade. which means "frontages" or "face"

### Flyweight 

The Flyweight pattern is a classical structural solution for optimizing code that is repetitive, slow, and inefficiently shares data. It aims to minimize the use of memory in an application by sharing as much data as possible with related objects (e.g application configuration, state, and so on).

### Proxy Design Pattern

There are times when it is necessary for us to control the access and context behind an object and this is where the Proxy pattern can be useful.

It can help us control when an expensive object should be instantiated, provide advanced ways to reference the object or modify the object to function a particular way in specific contexts.

#### 3. Behavioural Design Pattern

These types of Patterns recognize,implement, and improve communication between disparate objects in a system. They help ensure that disparate parts of a system have synchronized inforfation.

Some categories of Behavioural Design Pattern are :

* Chain of responsibility
* Command
* Iterator
* Mediator
* Memento
* Observer
* State
* Strategy
* Visitor

### Chain of responsibility
He chain of responsibility pattern allows passing requests along a chain of objects that have a chance to handle the request. Upon receiving a request, each handler decides either to process the request or to pass it to the next handler in the chain.

### Command 

The command design pattern encapsulates method invocation, operations, or requests into a single object so that we can pass method calls at our discretion. The command design pattern gives us an opportunity to issue commands from anything executing commands and delegates responsibility to different objects instead. These commands are presented in run() and execute() format.

### Iterator 

The interpreter pattern allows creating a grammar for simple language when a problem occurs very often; it could be considered to represent it as a sentence in a simple language so that an interpreter can solve the problem by interpreting the sentence.

### Mediator

The mediator pattern allows reducing chaotic dependencies between objects by defining an object that encapsulates how a set of objects interact.

### Memento

The memento pattern allows capture and externalizes an object’s internal state so that the object can be restored to this state later.

### Observer

The observer pattern allows defining a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

### State

The state pattern allows an object to alter its behavior when its internal state changes.

### Strategy
The strategy pattern allows defining a family of algorithms, encapsulate each one, and make them interchangeable.

### Visitor

The visitor pattern allows separate algorithms from the objects on which they operate.

### 4.Concurrency Design Pattern

These types of design patterns deal with multi-threaded programming paradigms. 

Some of the popular ones are:
* Active object
* Nuclear reaction
* Scheduler

### Components of the MVC Pattern
The components of the MVC Pattern are as follows.

#### Model
The model stores data and communicates directly with the database. This is where all data and related logic for the application is stored. It is responsible for defining business rules that govern data handling and how data is modified or processed. The model is not aware of the controller and view and usually notifies its observers of any changes in the model.

#### View
The view displays the model’s data and is responsible for how the data is represented in the user interface and how the end-users interact with the application. The view is the visual representation of the model’s data shown in grids, tables, charts, diagrams, forms, and other similar elements.

#### Controller
The controller controls how a user interacts in MVC applications. Controllers process incoming requests and handle user interactions and input. They also execute the appropriate application logic. In other words, the controller receives the input from the user, verifies the input data, and then executes the appropriate business logic that changes the data model’s state.

### 5 Architectural Design Patterns
Design patterns which are used for architectural purposes.

Some of the most famous ones are:
* MVC (Model-View-Controller)
* MVP (Model-View-Presenter)
* MVVM (Model-View-ViewModel)

### MVC (Model-View-Controller):

The Model View Controller (MVC), an architectural pattern, decouples an application’s presentation and business logic layers. It embodies three components: the model, the view, and the controller. The model represents the application’s data, the view displays data and the controller updates the view when the data changes.

The model has no understanding of the view or the controller. It usually alerts its observers when there is a change.

The primary goal of the MVC pattern is to separate concerns to ease testability. The MVC pattern allows you to segregate the problems while making your application’s code simpler to test and maintain. 

The request is routed via the controller in a typical MVC architecture, connecting the model to the relevant view. The MVC pattern is a compound pattern because while the view and controller leverage the strategy design pattern, the view and model are synchronized using the observer design pattern. The view subscribes to model changes. As the view and controller are decoupled, a controller can be used with several views in the MVC pattern.

### The benefits of the MVC pattern are:

* Clean separation of concerns
* Makes it easier to test the application’s code
* Accelerated parallel development
* Promotes de-coupling of the application’s layers
* Facilitates Better code organization, extensibility, and reuse.

### Albeit the advantages, the MVC pattern has certain downsides as well:

*It is quite complex to implement
*It is not a good choice for small applications

### MVP (Model-View-Presenter):

Like the MVC pattern, there are three components in the MVP pattern as well. These are the model, the view, and the presenter. The presenter replaces the Controller (in MVC) in the MVP design pattern. The MVP pattern allows for easier mocking of the view and more efficient unit testing of applications. In the MVP pattern, the presenter manipulates the model while simultaneously updating the view.

When your application must support multiple user interface technologies, the MVP design pattern is better suited than the MVC pattern. It is also a good choice if your user interface is complicated and requires a lot of user interaction. The MVP design pattern is better suited for automated unit testing of your app’s user interface than the traditional MVC design.

## Variations of the MVP Design Pattern

The MVP design pattern has two variations: 

* Passive View – in this strategy the view is not aware of the model, and it is updated by the presenter.

* Supervising Controller – in this strategy the view interacts directly with the model to bind data to data controls. The presenter updates the model and manipulates the view only if necessary.

### The MVP design pattern provides the following benefits:

* Clear separation of concerns
* Modularity
* Easier to test code

### MVVM (Model-View-ViewModel)

The ModelView-ViewModel (MVVM) design pattern is a variation of Martin Fowler’s Presentation Model Design Paradigm that builds on the popular MVC pattern. The ViewModel helps separate the presentation layer. The presenter contains the business logic in the MVVM design pattern, and the view is isolated from the model. 

The view component is aware of the presenter even if the presenter is oblivious of the view. The presenter represents an abstract view of the user interface. A passive view has no understanding of the model. The view is active in the MVVM design pattern and includes actions, events, and data binding information. 

### The MVVM design pattern provides the following benefits:

* Better separation of concerns
* Code reuse
* Easier to test and maintain

# Summary

## Design Pattern:

Design Pattern are evolved as reusable solutions to the problems that we encounter every day of programming.


## Types of Design Pattern

1. Creational Design Pattern
2. Structural Design Pattern
3. Behavioural Design Pattern
4. Concurrency Design Pattern
5. Architectural Design Pattern

# References
* https://www.freecodecamp.org/news/javascript-design-patterns-explained/ 
* https://www.dofactory.com/javascript/design-patterns
* https://www.youtube.com/watch?v=NU_1StN5Tkk

















